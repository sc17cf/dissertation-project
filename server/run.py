from app import app
from pyledgerserver import NetworkManager

if __name__ == '__main__':
      manager = NetworkManager()
      app.run(host='0.0.0.0', port=8000, debug=True)
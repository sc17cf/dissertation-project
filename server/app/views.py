from app import app
from flask_restful import Resource, Api
from flask import render_template, jsonify, request
import json
import os
import nacl.utils
import nacl.encoding
import nacl.signing
import hashlib

def getKeys():
    """
    Returns signing key and verifying key for the network.
    """

    sk = None
    vk = None
    try:        
        skDecoded = os.environ['sk']
        sk = str.encode(skDecoded)
        vkDecoded = os.environ['vk']
        vk = str.encode(vkDecoded)
    except Exception:
        print("Keys not found. Initialise the network manager and restart the server")

    return sk, vk

def validate(credentials):
    """
    *** Can be changed ***
    Validates the credentials
    

    """
    return True

def signJson(input):
    """
    *** Do not change ***
    Signs json to be and returns json certificate
    """
    sk, vk = getKeys()
    signer = nacl.signing.SigningKey(sk, encoder=nacl.encoding.HexEncoder)

    message = json.dumps(input, sort_keys=True)
    message = ''.join(message.split())

    sha512 = hashlib.sha512(message.encode('utf-8'))
    signed = signer.sign(sha512.digest())

    credentials = json.loads(message)
    responce = {
        "credentials":credentials,
        "signed":signed.signature.hex(),
        "operator":signer.verify_key.encode(encoder=nacl.encoding.HexEncoder).decode('utf-8')
    }
    return responce

@app.route('/join', methods=['POST'])
def index():
    """
    *** Do not change ***
    Validates credentials and issues digital certificate

    Credentials Provided:
        vk                  verfying key (public key)
        reg                 Valid reg
    """
    data = request.get_json(force=True)

    try:
        credentials = data['credentials']
    except Exception as e:
        return "Bad format", 400

    if validate(credentials):
        """
        peer has provided valid credentials so a certificate can be created.
         - Retrieve server keys
         - sign the provided credentials
         - Return certificate
        """

        responce = signJson(credentials)
        print(" >>> Credentials accepted. Sending signature.")
        
        return jsonify({'responce': responce}), 201
    else:
        return "credentials Denied", 403




                
            

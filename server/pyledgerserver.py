import os.path
from os import path
import nacl.utils
import nacl.encoding
import nacl.signing
import json
from datetime import datetime

class NetworkManager:
    def __init__(self):
        """
        Checks to see if a network has been initialised.
        If a network has been initialised and the config file format correct, load the variables.
        Else, create a new network
        """
        self.println("Starting ledger network manager V1.0.")

        if (path.exists('config.json')):
            with open('config.json') as file:
                data = json.load(file)
                try:
                    os.environ['sk'] = data['sk']
                    os.environ['vk'] = data['vk']
                    self.println("Network manager initialised.")
                except Exception:
                    self.println("Corrupt config file. Network manager cannot start.")
        else:
            self.create()

    def create(self):
        """
        Creates a new configuration file containing network information
        sk - signing key
        vk - verify key
        """

        self.println("No profile found. Creating new profile.")

        sk = nacl.signing.SigningKey.generate()
        vk = sk.verify_key

        data = {
            "sk":sk.encode(encoder=nacl.encoding.HexEncoder).decode('utf-8'),
            "vk":vk.encode(encoder=nacl.encoding.HexEncoder).decode('utf-8')
        }

        with open('config.json', 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)

    def println(self, message):
        print("   >>> "+datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " - " + message)




if __name__ == "__main__":
    net = NetworkManager()
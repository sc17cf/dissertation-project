import matplotlib.pyplot as plt
import matplotlib.ticker as mticker

"""
X and Y lists have been produced by the run.py file, changing the number of sessions and vehicles.
"""

x = [1,2,3,4,5,6,7,8]
y = [0.1839, 0.0771, 0.04202, 0.03020, 0.02329, 0.02530, 0.01659, 0.01905]

plt.plot(x,y)
plt.xlabel('Number of Sessions')
plt.ylabel('Mean Transaction Time (Seconds per transaction)')
plt.title('Transaction Throughput (Number of Vehicles = 20)')
plt.show()


x2 = [1,2,3,4,5,6,7,8,9,10,12,14,16,18,20]
y2 = [0.009030, 0.01401, 0.01914, 0.02659, 0.03301, 0.03954, 0.04637, 0.05360, 0.06567, 0.07665, 0.08486, 0.1081, 0.1317, 0.1580, 0.1913]


plt.plot(x2,y2)
plt.gca().xaxis.set_major_locator(mticker.MultipleLocator(1))
plt.xlabel('Participants per Session ')
plt.ylabel('Mean Transaction Time (Seconds per transaction)')
plt.title('Transaction Throughput')
plt.show()

x = [2,4,6,8,10,12,14,16,18,20]
y1 = [0.009030, 0.01914, 0.03301, 0.04637, 0.06567, 0.08486, 0.1081, 0.1317, 0.1580, 0.1913]
y2 = [1026.452, 486.452, 340.652, 216.452,162.452, 194.825, 113.852, 130.052, 146.252, 27.452]

fig, ax1 = plt.subplots()



color = 'tab:red'
ax1.set_title('Transaction throughput and Size of the Complete Ledger')
ax1.set_xlabel('Participants per Session')
fig.gca().xaxis.set_major_locator(mticker.MultipleLocator(1))
ax1.set_ylabel('Size of the Complete Ledger (kilobytes)', color=color)
ax1.plot(x, y2, color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('Mean Transaction Time (Seconds per Transaction)', color=color)  # we already handled the x-label with ax1
ax2.plot(x, y1, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.show()
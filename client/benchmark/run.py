import sys
sys.path.append('../pyledger')

from pyledger.core.Ledger import Ledger
from pyledger.sessions.SessionBuilder import SessionBuilder
from pyledger.sessions.SessionManager import SessionManager
from pyledger.communication.handler import IPCHandler
from pyledger.networkmanager.manage import NetManager
from pyledger.sessions.Transaction import Transaction
import time
import os
import shutil
import threading

global timings, data
timings = []

data = {
    "coordinates": [
    [
      [
        -1.5586424,
        53.808824
      ],
      [
        -1.5587577,
        53.8086815
      ],
      [
        -1.5588865,
        53.8085611
      ],
      [
        -1.5589991,
        53.8084566
      ],
      [
        -1.5591279,
        53.808333
      ],
      [
        -1.5592352,
        53.8082285
      ],
      [
        -1.5592352,
        53.8082285
      ],
      [
        -1.5592352,
        53.8082285
      ],
      [
        -1.5592352,
        53.8082285
      ],
      [
        -1.5592352,
        53.8082285
      ]
    ]
  ]
}

class Vehicle:
    def __init__(self, identifier, session):
        self.identifier = identifier
        self.ledger = Ledger(identifier)
        self.handler = IPCHandler()
        self.sessionId = session
        self.session = SessionManager(self.ledger, self.handler)
        self.port = self.handler.port

class Test:
    def __init__(self, n, m):
        """
        Create n vehicles.
        Initialise them to the network.
        Randomly add each vehicle to a session.
        run the communication.
        plot the result.
        """
        self.sessions = []
        self.vehicles = []
        for i in range(m):
            self.sessions.append({})

        net = NetManager()
        for i in range (n):
            identifier = net.join('http://localhost:8000', credentials = '{ "credentials":{"n":'+str(i)+'}}')
            session = i % m
            vehicle = Vehicle(identifier, session)
            self.vehicles.append(vehicle)

            vk = vehicle.ledger.getCertificate()['credentials']['vk']
            self.sessions[session][vk] = vehicle.port
            print('Vehicle',identifier,'setup on port',vehicle.port,'in session',session)

        print(self.vehicles)

        for vehicle in self.vehicles:
            if self.sessions[vehicle.sessionId] is None:
                  pass
              
            #Start the threads for each vehicle

            participants = self.sessions[vehicle.sessionId]
            x = threading.Thread(target=self.__start, args=(vehicle, participants))
            self.sessions[vehicle.sessionId] = None
            x.start()


        time.sleep(30)
        try:
            print(" >>> B: Average transaction throughput:" , sum(timings) / len(timings) )
        except Exception:
            print('not good')

  



    def __start(self, vehicle, participants):
        """
        Logic for each vehicle instance.
        Creates a update transactions and proposes them to the session that this vehicle is apart of.
        """
        
        if participants is not None:
            outs = {"location":None}
            genesis = Transaction(None, outs, participants)
            vehicle.session.new(genesis)

        #while len(vehicle.session.getsessionList()) < 1:
        #      time.sleep(2)

        for location in data['coordinates'][0]:
            update = { "location__"+vehicle.identifier: {"lng":location[0], "lat":location[1]} }
            
            #Issue the update
            for id in vehicle.session.getsessionList():
                tx = vehicle.session.update(update, id)
                t0 = time.time()
                vehicle.session.propose(tx)
                t1 = time.time()
                timings.append(t1-t0)

        print(vehicle.identifier, 'complete.')

  

    def clean(self):
        for vehicle in self.vehicles:
            path = vehicle.identifier
            shutil.rmtree(path)
            print('Removed ', path)


if __name__ == "__main__":
    benchmark = Test(22,1)


import json
import matplotlib.pyplot as plt
from geopy.distance import geodesic
import sys

colours = {
    'A':'b',
    'B':'g',
    'C':'r',
    'D':'c'
}


class Analyser:
    def __init__(self, files):
        self.ledgerList = {}
        self.identifiers = files
        
        for identifier in self.identifiers:
            fledger = '../'+identifier+'/ledger.json'

            with open(fledger) as file:
                self.ledgerList[identifier] = json.load(file)

        self.plotSpeeds()

    
    def plotSpeeds(self):
        data = {}
        for identifier in self.identifiers:
            data[identifier] = {}
            for session in list(self.ledgerList[identifier].keys()):
                for tx in self.ledgerList[identifier][session]:
                    if tx['outputs'] is not None:
                        variable = list(tx['outputs'].keys())[0]
                        vehicle = variable.split('__')[1]

                        if tx['outputs'][variable] is not None:
                            if vehicle not in list(data[identifier].keys()):
                                data[identifier][vehicle] = []

                            data[identifier][vehicle].append([tx['timestamp'],tx['outputs'][variable]['lng'], tx['outputs'][variable]['lat']])

        n=len(list(data.keys()))
        i =0


        fig = plt.figure(figsize=(12,15))

        for identifier in list(data.keys()):
            try:
                ax1 = fig.add_subplot(n,1,i+1, sharex = ax1)
            except Exception:
                ax1 = fig.add_subplot(n,1,i+1)


            i+=1
            print(identifier)
            for vehicle in list(data[identifier].keys()):
                deltaV = []
                time = []

                values = list(data[identifier][vehicle])
         
                values.sort(key=lambda x:x[0])
                print(values)

                for j in range(len(values)-1):

                    origin = (values[j][1], values[j][2])
                    dest = (values[j+1][1], values[j+1][2])

                    if (values[j+1][0] - values[j][0]) < 0.5:
                        continue
          
                    try:
                        print(geodesic(origin, dest).meters/(values[j+1][0] - values[j][0]), " m/s (",j,")" )
                        deltaV.append((geodesic(origin, dest).meters)/(values[j+1][0] - values[j][0]))
                    except Exception:
                        deltaV.append(0)

                    time.append(values[j+1][0])
                
                ax1.set_title(identifier)
                ax1.plot(time, deltaV, colours[vehicle]+'x-', label=vehicle,)
                ax1.legend(loc="upper left")



        fig.text(0.5, 0.01, 'time (unix time)', ha='center')
        fig.text(0.01, 0.5, 'Speed assuming average acceleration (m/s)', va='center', rotation='vertical')
        plt.tight_layout(pad=2.5)
        plt.savefig('plot.png')
        plt.show()
                




        





if __name__ == '__main__':
    files = ['7a46f8efa4a4176eda6a', '8cd693c0efcd58550530', '5ec6d179a90b5215c10c', 'c91347575e3ce56e34c9']
    analyser = Analyser(files)


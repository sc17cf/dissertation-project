import sys
sys.path.append('../pyledger')

from pyledger.core.Ledger import Ledger
from pyledger.sessions.SessionBuilder import SessionBuilder
from pyledger.sessions.SessionManager import SessionManager
from pyledger.communication.handler import IPCHandler
import time

def main():
    """
    Demonstration file

    Sets up the protocol and communicates with other vehicles.
    """ 
    ledger = Ledger('5ec6d179a90b5215c10c')
    handler = IPCHandler(5557)
    session = SessionManager(ledger, handler)

    time.sleep(3)
    timings = []
    
    for location in data['coordinates'][0]:
        """
        Sample Data
        In a production implementation this data would be sampled from the vehicles sensors.
        An update is then formed from the new data and proposed in a new transaction.
        """
        update = {
            "location__C": {"lng":location[0], "lat":location[1]}
        }
        
        for id in session.getsessionList():
            #time.sleep(2)
            tx = session.update(update, id)
            print(" >>> C: Proposal made")
            t0 = time.time()
            session.propose(tx)
            t1 = time.time()
            timings.append(t1-t0)

    print(" >>> C: Finished")
    try:
        print(" >>> C: Average transaction throughput:" , sum(timings) / len(timings) )
    except Exception:
        pass
    ledger.validate()

data = {
    "coordinates": [
    [
      [
        -1.5605253,
        53.8082412
      ],
      [
        -1.5603858,
        53.8081968
      ],
      [
        -1.5602142,
        53.8081556
      ],
      [
        -1.5600747,
        53.8081113
      ],
      [
        -1.559903,
        53.8080638
      ],
      [
        -1.5597421,
        53.8080289
      ],
      [
        -1.559608,
        53.8080226
      ],
      [
        -1.559608,
        53.8080226
      ],
      [
        -1.559608,
        53.8080226
      ],
      [
        -1.559608,
        53.8080226
      ]
    ]
  ]
}

if __name__ == "__main__":
    main()
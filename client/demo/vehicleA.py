import sys
sys.path.append('../pyledger')

from pyledger.core.Ledger import Ledger
from pyledger.sessions.SessionBuilder import SessionBuilder
from pyledger.sessions.SessionManager import SessionManager
from pyledger.communication.handler import IPCHandler
import time

def main():
    """
    Demonstration file

    Sets up the protocol and communicates with other vehicles.
    """
    
    time.sleep(1)
    ledger = Ledger('c91347575e3ce56e34c9')
    handler = IPCHandler(5555)
    builder = SessionBuilder(ledger, handler)
    session = builder.create(outputs, participants)

    time.sleep(2)
    timings = []

    for location in data['coordinates'][0]:
        """
        Sample Data
        In a production implementation this data would be sampled from the vehicles sensors.
        An update is then formed from the new data and proposed in a new transaction.
        """
        update = {
            "location__A": {"lng":location[0], "lat":location[1]}
        }
        
        for id in session.getsessionList():
            
            tx = session.update(update, id)
            print(" >>> A: Proposal made")
            t0 = time.time()
            session.propose(tx)
            t1 = time.time()
            timings.append(t1-t0)
           

    print(" >>> A: Finished")
    try:
        print(" >>> A: Average transaction throughput:" , sum(timings) / len(timings) )
    except Exception:
        pass
    ledger.validate()

data = {
    "coordinates": [
    [
      [
        -1.5586424,
        53.808824
      ],
      [
        -1.5587577,
        53.8086815
      ],
      [
        -1.5588865,
        53.8085611
      ],
      [
        -1.5589991,
        53.8084566
      ],
      [
        -1.5591279,
        53.808333
      ],
      [
        -1.5592352,
        53.8082285
      ],
      [
        -1.5592352,
        53.8082285
      ],
      [
        -1.5592352,
        53.8082285
      ],
      [
        -1.5592352,
        53.8082285
      ],
      [
        -1.5592352,
        53.8082285
      ]
    ]
  ]
}

outputs = {
        "location__A":None,
        "location__B":None,
        "location__B":None,
        }

participants = {"aab7934cb67e4fbcdeb9cef0b8b35370611cdf4e4feb0c6474fcb2c8720855d9":5556,
                "f71522d72a1571eeadb4b69976f9ccef3072d3e1419112fc5f539f5d8f91bbff":5557}


if __name__ == "__main__":
    main()
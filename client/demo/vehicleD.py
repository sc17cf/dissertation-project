import sys
sys.path.append('../pyledger')

from pyledger.core.Ledger import Ledger
from pyledger.sessions.SessionBuilder import SessionBuilder
from pyledger.sessions.SessionManager import SessionManager
from pyledger.communication.handler import IPCHandler
import time

def main():
    """
    Demonstration file

    Sets up the protocol and communicates with other vehicles.
    """ 

    ledger = Ledger('8cd693c0efcd58550530')
    handler = IPCHandler(5550)
    builder = SessionBuilder(ledger, handler)
    session = builder.create(outputs, participants)

    time.sleep(2)
    timings = []

    for location in data['coordinates'][0]:
        """
        Sample Data
        In a production implementation this data would be sampled from the vehicles sensors.
        An update is then formed from the new data and proposed in a new transaction.
        """
        update = {
            "location__D": {"lng":location[0], "lat":location[1]}
        }
        
        for id in session.getsessionList():
            #time.sleep(2)
            tx = session.update(update, id)
            print(" >>> D: Proposal made")
            t0 = time.time()
            session.propose(tx)
            t1 = time.time()
            timings.append(t1-t0)

    print(" >>> D: Finished")
    try:
        print(" >>> D: Average transaction throughput:" , sum(timings) / len(timings) )
    except Exception:
        pass
    ledger.validate()

data = {
    "coordinates": [
    [
      [
        -1.55927,
        53.8079513
      ],
      [
        -1.5593639,
        53.8079592
      ],
      [
        -1.5594953,
        53.8079656
      ],
      [
        -1.5596563,
        53.8079719
      ],
      [
        -1.5598091,
        53.8080004
      ],
      [
        -1.5599433,
        53.8080321
      ],
      [
        -1.5600747,
        53.8080701
      ],
      [
        -1.560182,
        53.8081002
      ],
      [
        -1.5603161,
        53.8081366
      ],
      [
        -1.5604717,
        53.8081746
      ]
    ]
  ]
}

outputs = {
        "location__A":None,
        "location__D":None
        }

participants = {"4502eb1a8b98063f781c9d99ff8d4a0fd17864b1998063bb87a91d8f9c4fb2db":5555}

if __name__ == "__main__":
    main()

import sys
sys.path.append('../pyledger')

from pyledger.core.Ledger import Ledger
from pyledger.sessions.SessionBuilder import SessionBuilder
from pyledger.sessions.SessionManager import SessionManager
from pyledger.communication.handler import IPCHandler
import time

def main():
    """
    Demonstration file

    Sets up the protocol and communicates with other vehicles.
    """
    ledger = Ledger('7a46f8efa4a4176eda6a')
    handler = IPCHandler(5556)
    session = SessionManager(ledger, handler)

    time.sleep(3)
    timings = []

    for location in data['coordinates'][0]:
          
        """
        Sample Data
        In a production implementation this data would be sampled from the vehicles sensors.
        An update is then formed from the new data and proposed in a new transaction.
        """
        update = {
            "location__B": {"lng":location[0], "lat":location[1]}
        }

        #Loop through all existing sessions and propose the update values.
        for id in session.getsessionList():
            tx = session.update(update, id)
            print(" >>> B: Proposal made")
            t0 = time.time()
            session.propose(tx)
            t1 = time.time()
            timings.append(t1-t0)
            time.sleep(1)
    
    print(" >>> B: Finished")
    try:
        print(" >>> B: Average transaction throughput:" , sum(timings) / len(timings) )
    except Exception:
        pass
    
    ledger.validate()
    



data = {
    "coordinates": [
    [
      [
        -1.5605307,
        53.806632
      ],
      [
        -1.5603644,
        53.8069234
      ],
      [
        -1.5601927,
        53.80718
      ],
      [
        -1.5600103,
        53.8074651
      ],
      [
        -1.559844,
        53.8076837
      ],
      [
        -1.5597045,
        53.8078705
      ],
      [
        -1.5596133,
        53.8080036
      ],
      [
        -1.5596133,
        53.8080036
      ],
      [
        -1.5596133,
        53.8080036
      ],
      [
        -1.5596133,
        53.8080036
      ]
    ]
  ]  
}


if __name__ == "__main__":
    main()
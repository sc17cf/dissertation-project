import threading
import time

from . import vehicleA
from . import vehicleB
from . import vehicleC
from . import vehicleD

def run():

    A = threading.Thread(target=vehicleA.main)
    A.start()

    B = threading.Thread(target=vehicleB.main)
    B.start()

    C = threading.Thread(target=vehicleC.main)
    C.start()

    D = threading.Thread(target=vehicleD.main)
    D.start()

    

if __name__ == "__main__":
    run()
from .Transaction import Transaction
from .SessionManager import SessionManager
from pyledger.core.Exceptions import InvalidTransactionData
import nacl.utils
import nacl.encoding
import nacl.signing

class SessionBuilder:
    def __init__(self, ledger, handler):
        self.ledger = ledger
        self.__id = None
        self.__sessionManager = None
        self.__handler = handler

    def create(self, outputs, signers):
        """
        Gets a genesis transaction, appends local to the participants list.
        Generates a sessionManager with the genesis transaction
        """

        signer = self.ledger.getSigner()
        signers[signer.verify_key.encode(encoder=nacl.encoding.HexEncoder).decode('utf-8')] = self.__handler.port
        self.__genesis = Transaction(None, outputs, signers)
        self.__sessionManager = SessionManager(self.ledger, self.__handler)
        self.__sessionManager.new(self.__genesis)

        return self.__sessionManager



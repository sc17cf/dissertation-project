from pyledger.core.Crypto import Crypto
from pyledger.core.Exceptions import BadTransactionSignature, BadCredentialsError
from pyledger.sessions.Transaction import Transaction

import nacl.utils
import nacl.encoding
import nacl.signing
import json
import requests
import hashlib
import time

class SessionManager:
    def __init__(self, ledger, handler):
        self.__id = None
        self.__ledger = ledger
        self.__crypto = Crypto()
        self.__handler = handler
        self.__handler.run(self)
        self.__sessionList = []
        self.__sessionMeta = {}
        
    def new(self, genesis):
        """
        Takes a genesis transaction to set the initial state and creates a new session.
        """
        genesis.setTime()

        if (self.verifyCredentials(self.__ledger.getCertificate())):
            self.sign(genesis)
            self.collectSignatures(genesis)
            self.complete(None, genesis, self.__handler.port)
            self.broadcastComplete(genesis)
        else:
            print(' >>> InvalidCredentials')

    def collectSignatures(self, tx):
        """
        Distributes the signed genesis transaction 
        commits the genesis transaction to the ledger
        """

        sign = {
            "sign":tx.serialize(),
            "credentials":self.__ledger.getCertificate(),
            "session":tx.session
        }

        for vk in tx.participants:
            if not vk == self.__ledger.getCertificate()['credentials']['vk']:
                responce = self.__handler.sendRecieve(int(tx.participants[vk]), sign)
                responce = json.loads(responce.decode('utf-8'))
                self.verifyCredentials(responce['credentials'])
                vk = list(responce['signature'].keys())[0]
                tx.addSignature(responce['signature'][vk], vk)

    def sign(self, tx):
        """
        Takes a transaction and signs it.
        Converts the transaction to a json string and encodes the string into bytes.
        These bytes are signed, decoded to Hex and finally decoded to UTF-8
        """
        message = tx.signSerialize()
        message = json.dumps(message)
        message = ''.join(message.split())

        sha512 = self.__crypto.hashMessage(message.encode('utf-8'))
        
        signer = self.__ledger.getSigner()
        signed = signer.sign(sha512.digest())
        vk = signer.verify_key.encode(encoder=nacl.encoding.HexEncoder).decode('utf-8')
        tx.signatures[vk] = signed.signature.hex()

        print('         - signed by ',self.__ledger.getCertificate()['credentials']['vk'])

        return {vk:signed.signature.hex()}

    def complete(self, session, tx, sender):
        """
        Commit the transaction forging the ID and add it to the ledger
        """
        tx.commit()
        if tx.session == None:
            tx.session = tx.id

        if tx.session not in self.__sessionList:
            print(' >>> New session '+tx.session+' detected')
            self.__sessionList.append(tx.session)

        print('         - Completed by ',self.__ledger.getCertificate()['credentials']['vk'])
        self.__ledger.add(session, tx)
        self.setMeta(tx.session, sender)
    
    def broadcastComplete(self, tx):
        """
        Broadcast the completed transaction to all the participants in the session
        """
        complete = {
            "complete":tx.serialize(),
            "credentials":self.__ledger.getCertificate(),
            "session":tx.session,
            "sender":self.__handler.port
        }

        for vk in tx.participants:
            if not vk == self.__ledger.getCertificate()['credentials']['vk']:
                signature = self.__handler.sendRecieve(int(tx.participants[vk]), complete)


    def verifySignature(self, tx, signature, verifyKey):
        """
        Params -    message:    string
                    signature:  Hex String    
                    verifyKey:  Hex String 
        """
        verifyKey = str.encode(verifyKey)
        signature = bytes.fromhex(signature) 
        
        message = tx.signSerialize()
        message = json.dumps(message)
        message = ''.join(message.split())

        sha512 = self.__crypto.hashMessage(message.encode('utf-8'))

        return self.__crypto.verify(sha512.digest(), signature, verifyKey)

    def verifyCredentials(self, credentials):
        """
        Takes credentials as a dict object 
        Formats them to encoded or byte objects
        The message and signature objects are then verifyed
        """
        operator = str.encode(credentials['operator'])
        signature = bytes.fromhex(credentials['signed']) 

        message = json.dumps(credentials['credentials'])
        message = ''.join(message.split())

        sha512 = self.__crypto.hashMessage(message.encode('utf-8'))

        try:
            return self.__crypto.verify(sha512.digest(), signature, operator)
        except Exception:
            raise BadCredentialsError('Credentials are not valid.')

        return False

    def getsessionList(self):
        return self.__sessionList

    def update(self, update, id):
        """
        Given updates in json format produce a new transaction that updates the session with id = id.
        """
        head = self.__ledger.getHead(id)

        elapsed = 0
        waitFor = self.__getWaitForPort(head['participants'])
        while self.__sessionMeta[id] != waitFor:
            elapsed += 0.5
            time.sleep(0.5)
            if elapsed == 3:
                self.__sessionMeta[id] = waitFor

        self.__sessionMeta[id] = None
        head = self.__ledger.getHead(id)
        tx = Transaction(None, update, head['participants'])
        tx.session = id
        tx.previousHash = head['id']
        return tx

    def propose(self, tx):
        """
        Propose a transaction as an update to an existing session.
        
        """
        tx.setTime()
        if (self.verifyCredentials(self.__ledger.getCertificate())):
            self.sign(tx)
            self.collectSignatures(tx)
            self.complete(tx.session, tx, self.__handler.port)
            self.broadcastComplete(tx)
        else:
            print(' >>> InvalidCredentials')


    def setMeta(self, id, port):
        self.__sessionMeta[id] = port

    def __getWaitForPort(self, participants):
        ports = list(participants.values())
        pos = ports.index(self.__handler.port)
        pos = (pos+1) % len(ports)
        return ports[pos]

    def getCertificate(self):
        return self.__ledger.getCertificate()


        






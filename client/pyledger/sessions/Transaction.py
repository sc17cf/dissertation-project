from pyledger.core.Crypto import Crypto
from pyledger.core.Exceptions import BadTransactionSignature
import json
import time

class Transaction:
    """
    Representation of a transaction.
    """
        
    def __init__(self, inputs, outputs, participants):
        self.id = None
        self.inputs = inputs
        self.outputs = outputs
        self.timestamp = None
        self.participants = participants
        self.signatures = {}
        self.__crypto = Crypto()
        self.previousHash = None
        self.session = None
        
    def serialize(self):
        """
        Turns transaction into JSON data
        """
        json ={
            "id":self.id,
            "timestamp":self.timestamp,
            "previousTx":self.previousHash,
            "inputs":self.inputs,
            "outputs":self.outputs,
            "participants":self.participants,
            "signatures":self.signatures,
        }
        
        return json

    def signSerialize(self):
        json ={
            "id":self.id,
            "timestamp":self.timestamp,
            "previousTx":self.previousHash,
            "inputs":self.inputs,
            "outputs":self.outputs,
            "participants":self.participants,
        }        
        return json

    def commit(self):
        """
        Forge the id of the transaction
        """

        message = self.signSerialize()
        message = json.dumps(message)
        message = ''.join(message.split())

        sha512 = self.__crypto.hashMessage(message.encode('utf-8'))
        self.id = sha512.hexdigest()     

    def addSignature(self, signature, verifyKey):
        """
        Add a signature to the transaction
        """
        try:
            self.__verifySignature(signature, verifyKey)
        except BadTransactionSignature:
            return False

        self.signatures[verifyKey] = signature
        return True

    def __verifySignature(self, signature, verifyKey):
        """
        Input
                signature:      Hex String    
                verifyKey:      Hex String 
        Output 
                True:           Signature Added 
                False:          Not added
        
        """
        verifyKey = str.encode(verifyKey)
        signature = bytes.fromhex(signature) 
        
        message = self.signSerialize()
        message = json.dumps(message)
        message = ''.join(message.split())

        sha512 = self.__crypto.hashMessage(message.encode('utf-8'))

        return self.__crypto.verify(sha512.digest(), signature, verifyKey)

    def verifySignatures(self):
        """
        Verifys all signatures for a transaction are correct
        """
        for sig in self.signatures:
            self.__verifySignature(self.signatures[sig], sig)


    def setTime(self):
        self.timestamp = time.time()

    def printTx(self):
        print(json.dumps(self.serialize(), indent=4, sort_keys=True))




"""
Session( = sig(t0)):
    participants:
        v1, v2 ,v3
    transaction:
        t0 -> t1 -> t2 -> t3 -> t4

"""
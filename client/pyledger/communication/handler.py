import time
import zmq
import threading
import json
from pyledger.sessions.Transaction import Transaction

class IPCHandler:
    """
    - Sets up a simple interprocess messaging server on a new thread.
    - Handlers sending messages to over vehicles message servers.

    """
    def __init__(self, port = None):
        self.port = port
        context = zmq.Context()
        self.socket = context.socket(zmq.REP)


        if self.port is None:
            self.port = self.socket.bind_to_random_port('tcp://*', min_port=1000, max_port=6004, max_tries=100)
        else:
            self.socket.bind("tcp://*:%s" % str(self.port))

    def run(self, session):
        x = threading.Thread(target=self.__runServer, args=(session,))
        x.start()

    def __runServer(self, session):
        """
        Starts up the server, listens for requests
        Once a request has been found, pass it to the session manager
        """
        

        while True:
            message = self.socket.recv()
            responce ={}
        
            try:
                message.decode('utf-8')
                message = json.loads(message)
                if (session.verifyCredentials(message['credentials'])):
                    if 'sign' in message.keys():
                        responce = self.handleSign(message['sign'], session, message['session'])
                        
                    if 'complete' in message.keys():
                        responce = self.handleComplete(message['complete'], session, message['session'], message['sender'])
                else:
                    print(' >>> credentials failed')
            except Exception as e:
                pass

            self.socket.send_json(responce)


    def sendRecieve(self, port, message):
        """
        Send a message to another vehicle and return the responce
        """
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.connect("tcp://localhost:%s" % str(port))
        socket.send_json(message)

        responce = socket.recv()
        socket.close()
        context.destroy()
        return responce

    def handleSign(self, message, session, sessionId): 
        """
        Handlers a request to sign a transaction from another vehicle.

        The data is loaded into a transaction which is sent to the session manager for signing.
        """      
        tx = Transaction(message['inputs'],
                        message['outputs'],
                        message['participants'])

        tx.previousHash = message['previousTx']
        tx.session = sessionId
        tx.timestamp = message['timestamp']

        for vk in message['signatures']:
            tx.addSignature(message['signatures'][vk], vk)

        sig = session.sign(tx)

        responce = {"signature":sig,
                    "credentials": session.getCertificate()
                    } 

        return responce

    def handleComplete(self, message, session, sessionId, sender):
        """
        Handle a complete transaction request.

        Load the data into a transaction object.
        Send it to the session manager to be completed and return the responce.
        """
        tx = Transaction(message['inputs'],
                        message['outputs'],
                        message['participants'])

        tx.previousHash = message['previousTx']
        tx.session = sessionId
        tx.timestamp = message['timestamp']

        for vk in message['signatures']:
            tx.addSignature(message['signatures'][vk], vk)

        session.complete(sessionId, tx, sender)
        return {"success":"completed"}

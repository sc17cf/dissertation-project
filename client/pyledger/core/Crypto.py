import nacl.utils
import nacl.encoding
import nacl.signing
import hashlib
from pyledger.core.Exceptions import BadSignatureError

class Crypto:
    """
    Implements cryptographic algorithms by NaCl
    """
    def hashMessage(self, message):
        return hashlib.sha512(message)
        
    def verify(self, message, signature, publicKey):
        verifyKey = nacl.signing.VerifyKey(publicKey,
                                    encoder=nacl.encoding.HexEncoder)

        try:
            verifyKey.verify(message, signature)
        except BadSignatureError:
            return False
                                    
        return True
                    
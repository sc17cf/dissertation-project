import os.path
from os import path
from pyledger.core.Exceptions import LedgerForkException
from pyledger.sessions.Transaction import Transaction
import nacl.utils
import nacl.encoding
import nacl.signing
import json
import copy
import sys


class Ledger:
    def __init__(self, identifier):
        self.__init = False
        self.__sk = None
        self.__certificate = None
        self.__ledger = None
        self.__identifier= identifier
        self.load(identifier)

    def getSigner(self):
        return self.__sk

    def getCertificate(self):
        return self.__certificate

    def load(self, identifier):
        """
        Loads ledger and config files into json data structures which can be used by the session manager
        """
        fconfig = identifier+'/config.json'
        fledger = identifier+'/ledger.json'
        
        if os.path.exists(identifier) and os.path.exists(fconfig) and os.path.exists(fledger):
            try:
                with open(fconfig) as file:
                    config = json.load(file)
                    try:
                        self.__certificate = config['global']
                        sk = config['local']['sk']
                        sk = str.encode(sk)
                        self.__sk = nacl.signing.SigningKey(sk, encoder=nacl.encoding.HexEncoder)
                    except Exception:
                        raise IncompatibleFileFormat('Error loading certificate or secret key')
                with open(fledger) as file:
                    self.__ledger = json.load(file)
            except Exception:
                raise IncompatibleFileFormat('Error loading data from config or ledger dependencies')
        else:
            raise FileNotFoundError('Error loading '+identifier +'. Could not locate directory, config or ledger dependencies')

        if not self.validate():
            raise InvalidLedgerFormat('Error validating ledger structure')

    def validate(self):
        """
        Iterates through the leger JSON data structure and checks the following conditions are met

            - No forks are present in the ledger, i.e. a transaction id exists once in the previous transation fild.
            - Every signature matches the content of the transaction.

        """
        print(" >>> Checking ledger integrity")
        for session in self.__ledger.values():
            previous = None
            for tx in session:
                if not tx['previousTx'] == previous:
                    raise LedgerForkException('A fork has been detected in the ledger at transaction '+tx['id'])
                    sys.exit()
                    pass

                previous = tx['id']

                vTx = Transaction(tx['inputs'],
                        tx['outputs'],
                        tx['participants'])

                vTx.signatures = tx['signatures']
                vTx.previousHash = tx['previousTx']
                vTx.timestamp = tx['timestamp']
                 
                vTx.verifySignatures()

        print(" >>> Ledger validation complete.")

        return True

    def getHead(self, id):
        try:
            session = self.__ledger[id]
            head = self.__ledger[id][len(session)-1]

            return head
        except Exception:
            return None


    def add(self, session, tx):
        if session in self.__ledger:
            self.__ledger[session].append(tx.serialize())
        else:
            self.__ledger[tx.serialize()['id']] = [tx.serialize()]
        self.commit()

    def commit(self):
        file = self.__identifier+'/ledger.json'
        f = open(file, 'r+')
        f.truncate(0) # need '0' when using r+
        f.close()

        temp = copy.deepcopy(self.__ledger)

        with open(file, 'w', encoding='utf-8') as f:
            json.dump(temp, f, ensure_ascii=False, indent=4)

    
class IncompatibleFileFormat(Exception):
    pass

class InvalidLedgerFormat(Exception):
    pass

    
if __name__ == "__main__":
    net = Ledger('a0d7003402db2875e551')
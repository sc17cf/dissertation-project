class BadTransactionSignature(Exception):
    pass

class InvalidTransactionData(Exception):
    pass


class BadSignatureError(Exception):
    pass


class LedgerForkException(Exception):
    pass


class BadCredentialsError(Exception):
    pass
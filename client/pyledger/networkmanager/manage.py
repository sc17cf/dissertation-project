
import os.path
from os import path
import nacl.utils
import nacl.encoding
import nacl.signing
import json
import requests
import hashlib
from datetime import datetime

class NetManager:
    def __init__(self):
        self.println("Network manager starting up")

    def keyGenerator(self):
        """
        Generates a set of RSA public/private keys using NaCl
        """

        sk = nacl.signing.SigningKey.generate()
        vk = sk.verify_key

        keys = {
            "sk":sk.encode(encoder=nacl.encoding.HexEncoder).decode('utf-8'),
            "vk":vk.encode(encoder=nacl.encoding.HexEncoder).decode('utf-8')
        }

        return keys

    def join(self, url, credentials):
        """
        Make Ajax request to join a network.
        url - url to endpoint for joining network 
        credentials - json representation of the credentials
        """
        keys = self.keyGenerator()

        data = json.loads(credentials)
        data['credentials']['vk'] = keys['vk']
        credentials = json.dumps(data)

        joinUrl = url+'/join'
        responce = requests.post(url = joinUrl, data = credentials) 
        if responce.status_code == 201:
            self.println("credentials Accepted.")
        elif responce.status_code == 400:
            self.println("ERROR - Incompatible credentials format")
        elif responce.status_code == 403:
            self.println("ERROR - Credentials Denied.")
        else:
            self.println("ERROR - "+ responce.reason)

        identifier = self.create(json.loads(responce.content), url, keys)

        return identifier

    def create(self, signedCred, url, keys):
        """
        Creates the config and ledger file for the new network
        """

        #--- Setup json data structure
        data = {}
        data['global'] = signedCred['responce']
        data['global']['url'] = url
        data['local'] = keys
        identifier = data['global']['signed'][0:20]
        path = identifier
        print(path)
        # ----

        if not os.path.exists(path):
            os.mkdir(path)
            with open(path+'/config.json', 'w+', encoding='utf-8') as f:
                json.dump(data, f, ensure_ascii=False, indent=4)

            file = open(path+'/ledger.json', 'w+')
            file.write('{}')
            file.close()
            self.println(identifier+" Created ")
        else:    
            self.println(identifier+" already exists")
        
        return identifier

    def println(self, message):
        print("   >>> "+datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " - " + message)

    

if __name__ == "__main__":
    credentials = '{ "credentials":{"reg":"LP1 AD0","driverNumber":"BC20D0874399D"}}'
    net = NetManager()
    net.join("http://localhost:8000", credentials)



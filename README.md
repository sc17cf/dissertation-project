# Instructions

The 'pyledger' directory holds all of the python source code for the protocol implementation. A small demo of the system can be found in the 'demo' folder. Any folder thats name is a hash is the files needed for a node who has joined a network. These existing files are used by the demo.

Note: Docker is required to build this project. Alternatively, python3 must be installed with all the dependencies referenced in requirements.txt and client-requirements.txt.

## File Structure

* client/pyledger - python package implementation of the protocol. This is imported for communication.
* client/benchmark/run.py - small program that uses the protocol to investigate the transaction rate for a given sesssion size. The number of vehicles communicating and the number of sessions can be changed, communication will then run and the transaction speeds will be output.
* client/demo/run.py - The demo of four vehicles communicating on the network.
* server - flask server for the membership service.

## Running the Demo

* cd into the project directory containing the docker-compose.yml
* Run command:docker-compose up


## Running the Benchmark

* cd into the 'client' directory and open the launch.sh file.
* Change the line of code 'python3 -m demo.run' to 'python3 -m benchmark.run'.
* Open the file 'client/benchmark/run.py' and change the instantiation parameters at line 153.
* Run 'docker-compose up'

## Using the protocol

* Create a new python file and import the required classes from the pyledger class (see 'Extra Information' when in a sibiling directory to 'pyledger').
* Write the code that uses the protocol.
* change 'launch.sh' to point to the new file or package you created.
* Run 'docker-compose up'

## Extra information

When running the demo, benchmark or any other file in a sibiling directory level to the pyledger package, run the file as a package. An empty python file called __init__.py is required in the directory of the file you are running. 

Example: 
Do not use: python3 demo/run.py
use: python3 -m demo.run


